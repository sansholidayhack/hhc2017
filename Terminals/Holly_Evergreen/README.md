                     ___
                    / __'.     .-"""-.
              .-""-| |  '.'.  / .---. \
             / .--. \ \___\ \/ /____| |
            / /    \ `-.-;-(`_)_____.-'._
           ; ;      `.-" "-:_,(o:==..`-. '.         .-"-,
           | |      /       \ /      `\ `. \       / .-. \
           \ \     |         Y    __...\  \ \     / /   \/
     /\     | |    | .--""--.| .-'      \  '.`---' /
     \ \   / /     |`        \'   _...--.;   '---'`
      \ '-' / jgs  /_..---.._ \ .'\\_     `.
       `--'`      .'    (_)  `'/   (_)     /
                  `._       _.'|         .'
                     ```````    '-...--'`
My name is Holly Evergreen, and I have a conundrum.
I broke the candy cane striper, and I'm near throwing a tantrum.
Assembly lines have stopped since the elves can't get their candy cane fix.
We hope you can start the striper once again, with your vast bag of tricks.
Run the CandyCaneStriper executable to complete this challenge.

------------------------------------------------------------------------

# We've located the CandyCaneStriper

elf@e7468a72f04c:~$ ls -l 
total 96
-rw-r--r-- 1 root root 45224 Dec 15 19:59 CandyCaneStriper

# But:
#	- It doesn't have execute permission
#	- We don't own the file, so we can't alter it's permissions

# Using the file command, we discover it's a binary file

elf@e7468a72f04c:~$ file CandyCaneStriper 
CandyCaneStriper: ELF 64-bit LSB executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 2.6.32, BuildID[sha1]=bfe4ffd88f30e6970feb7e3341ddbe579e9ab4b3, stripped

# We locate the ld-linux Shared Object, which acts as the interpreter for these binaries.

elf@e7468a72f04c:~$ find / -name "ld-linux*"
find: '/var/cache/ldconfig': Permission denied
find: '/var/cache/apt/archives/partial': Permission denied
find: '/var/lib/apt/lists/partial': Permission denied
find: '/proc/tty/driver': Permission denied
find: '/etc/ssl/private': Permission denied
/lib/x86_64-linux-gnu/ld-linux-x86-64.so.2
find: '/root': Permission denied
/lib64/ld-linux-x86-64.so.2

# We Execute: ld-linux passing the filename of the CandyCaneStriper as a parameter:

elf@e7468a72f04c:~$ /lib64/ld-linux-x86-64.so.2 ./CandyCaneStriper
                   _..._
                 .'\\ //`,      
                /\\.'``'.=",
               / \/     ;==|
              /\\/    .'\`,`
             / \/     `""`
            /\\/
           /\\/
          /\ /
         /\\/
        /`\/
        \\/
         `
The candy cane striping machine is up and running!

