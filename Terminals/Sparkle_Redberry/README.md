                ___,@
               /  <
          ,_  /    \  _,
      ?    \`/______\`/
   ,_(_).  |; (e  e) ;|
    \___ \ \/\   7  /\/    _\8/_
        \/\   \'=='/      | /| /|
         \ \___)--(_______|//|//|
          \___  ()  _____/|/_|/_|
             /  ()  \    `----'
            /   ()   \
           '-.______.-'
   jgs   _    |_||_|    _
        (@____) || (____@)
         \______||______/


My name is Sparkle Redberry, and I need your help.
My server is atwist, and I fear I may yelp.
Help me kill the troublesome process gone awry.
I will return the favor with a gift before nigh.
Kill the "santaslittlehelperd" process to complete this challenge.

------------------------------------------------------------------------

Attempting to kill the process 'santaslittlehelperd' appears to silently fail.

This Challenge goes to show it's important to understand your environment

If you examine command aliases you'll see the kill commands have been aliased to true:
	alias
	alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(his
	tory|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
	alias egrep='egrep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias grep='grep --color=auto'
	alias kill='true'
	alias killall='true'
	alias l='ls -CF'
	alias la='ls -A'
	alias ll='ls -alF'
	alias ls='ls --color=auto'
	alias pkill='true'
	alias skill='true'

These aliases are defined in the users .bashrc file

One of the simplest solutions is to unalias the command:
	unalias kill 8


If you want to save a few characters typing, you could switch shell's to one that doesn't have the command aliasing
	sh
	kill 8

