                                 |
                               \ ' /
                             -- (*) --
                                >*<
                               >0<@<
                              >>>@<<*
                             >@>*<0<<<
                            >*>>@<<<@<<
                           >@>>0<<<*<<@<
                          >*>>0<<@<<<@<<<
                         >@>>*<<@<>*<<0<*<
           \*/          >0>>*<<@<>0><<*<@<<
       ___\\U//___     >*>>@><0<<*>>@><*<0<<
       |\\ | | \\|    >@>>0<*<0>>@<<0<<<*<@<<  
       | \\| | _(UU)_ >((*))_>0><*<0><@<<<0<*<
       |\ \| || / //||.*.*.*.|>>@<<*<<@>><0<<<
       |\\_|_|&&_// ||*.*.*.*|_\\db//_               
       """"|'.'.'.|~~|.*.*.*|     ____|_
           |'.'.'.|   ^^^^^^|____|>>>>>>|
           ~~~~~~~~         '""""`------'
My name is Bushy Evergreen, and I have a problem for you.
I think a server got owned, and I can only offer a clue.
We use the system for chat, to keep toy production running.
Can you help us recover from the server connection shunning?
Find and run the elftalkd binary to complete this challenge.

------------------------------------------------------------------------

This is a simple challenge to locate a file.
On servers that have locate / updatedb installed, locate provides a very fast way of locating files using a database, rather than having to trawl through the filesystem. As this isn't installed on this server, we'll use the find command.

# Our environment is attempting to run /usr/local/bin/find unsucesfully:

elf@bdace301bde5:~$ find / -name "elftalkd"
bash: /usr/local/bin/find: cannot execute binary file: Exec format error
elf@bdace301bde5:~$ ls -l /usr/local/bin/find
-rwxr-xrwx 1 root root 201088 Dec  4 14:29 /usr/local/bin/find

# If we explicitly specify /usr/bin/find , all is well:

elf@bdace301bde5:~$ /usr/bin/find / -name "elftalkd"
/usr/bin/find: '/var/cache/ldconfig': Permission denied
/usr/bin/find: '/var/cache/apt/archives/partial': Permission denied
/usr/bin/find: '/var/lib/apt/lists/partial': Permission denied
/run/elftalk/bin/elftalkd
/usr/bin/find: '/proc/tty/driver': Permission denied
/usr/bin/find: '/root': Permission denied

# Having found our file, we list it to see it's ownership and file permissions

elf@bdace301bde5:~$ ls -l /run/elftalk/bin/elftalkd
-rwxr-xr-x 1 root root 7385168 Dec  4 14:29 /run/elftalk/bin/elftalkd

# and then execute it:

elf@bdace301bde5:~$ /run/elftalk/bin/elftalkd
Running in interactive mode
        --== Initializing elftalkd ==--
Initializing Messaging System!
Nice-O-Meter configured to 0.90 sensitivity.
Acquiring messages from local networks...
--== Initialization Complete ==--
      _  __ _        _ _       _ 
     | |/ _| |      | | |     | |
  ___| | |_| |_ __ _| | | ____| |
 / _ \ |  _| __/ _` | | |/ / _` |
|  __/ | | | || (_| | |   < (_| |
 \___|_|_|  \__\__,_|_|_|\_\__,_|
-*> elftalkd! <*-
Version 9000.1 (Build 31337) 
By Santa Claus & The Elf Team
Copyright (C) 2017 NotActuallyCopyrighted. No actual rights reserved.
Using libc6 version 2.23-0ubuntu9
LANG=en_US.UTF-8
Timezone=UTC
Commencing Elf Talk Daemon (pid=6021)... done!
Background daemon...

