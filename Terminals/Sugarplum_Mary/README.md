
                       *
                      .~'
                     O'~..
                    ~'O'~..
                   ~'O'~..~'
                  O'~..~'O'~.
                 .~'O'~..~'O'~
                ..~'O'~..~'O'~.
               .~'O'~..~'O'~..~'
              O'~..~'O'~..~'O'~..
             ~'O'~..~'O'~..~'O'~..
            ~'O'~..~'O'~..~'O'~..~'
           O'~..~'O'~..~'O'~..~'O'~.
          .~'O'~..~'O'~..~'O'~..~'O'~
         ..~'O'~..~'O'~..~'O'~..~'O'~.
        .~'O'~..~'O'~..~'O'~..~'O'~..~'
       O'~..~'O'~..~'O'~..~'O'~..~'O'~..
      ~'O'~..~'O'~..~'O'~..~'O'~..~'O'~..
     ~'O'~..~'O'~..~'O'~..~'O'~..~'O'~..~'
    O'~..~'O'~..~'O'~..~'O'~..~'O'~..~'O'~.
   .~'O'~..~'O'~..~'O'~..~'O'~..~'O'~..~'O'~
  ..~'O'~..~'O'~..~'O'~..~'O'~..~'O'~..~'O'~.
 .~'O'~..~'O'~..~'O'~..~'O'~..~'O'~..~'O'~..~'
O'~..~'O'~..~'O'~..~'O'~..~'O'~..~'O'~..~'O'~..
Sugarplum Mary is in a tizzy, we hope you can assist.
Christmas songs abound, with many likes in our midst.
The database is populated, ready for you to address.
Identify the song whose popularity is the best.
total 20684
------------------------------------------------------------------------

# Se we've got a SQLite database file:

elf@5075a036ad10:~$ ll
total 20704
drwxr-xr-x 1 elf  elf      4096 Dec  7 15:13 ./
drwxr-xr-x 1 root root     4096 Dec  4 14:33 ../
-rw-r--r-- 1 elf  elf       220 Aug 31  2015 .bash_logout
-rw-r--r-- 1 root root     3898 Dec  4 14:29 .bashrc
-rw-r--r-- 1 elf  elf       655 May 16  2017 .profile
-rw-r--r-- 1 root root 15982592 Nov 29 19:28 christmassongs.db
-rwxr-xr-x 1 root root  5197352 Dec  7 15:10 runtoanswer*

# Lets check where the sqlite binary is:

elf@5075a036ad10:~$ find / -name "*sqlite*"
/usr/bin/sqlite3
/usr/share/doc/sqlite3
/usr/share/doc/libsqlite3-0
/usr/share/man/man1/sqlite3.1.gz
/usr/lib/x86_64-linux-gnu/libsqlite3.so.0.8.6
/usr/lib/x86_64-linux-gnu/libsqlite3.so.0
find: '/var/cache/ldconfig': Permission denied
find: '/var/cache/apt/archives/partial': Permission denied
/var/lib/dpkg/info/libsqlite3-0:amd64.postinst
/var/lib/dpkg/info/libsqlite3-0:amd64.symbols
/var/lib/dpkg/info/libsqlite3-0:amd64.list
/var/lib/dpkg/info/sqlite3.md5sums
/var/lib/dpkg/info/libsqlite3-0:amd64.shlibs
/var/lib/dpkg/info/libsqlite3-0:amd64.triggers
/var/lib/dpkg/info/sqlite3.list
/var/lib/dpkg/info/libsqlite3-0:amd64.md5sums
find: '/var/lib/apt/lists/partial': Permission denied
find: '/proc/tty/driver': Permission denied
find: '/root': Permission denied

# Using SQLite we:
# - open the christmas songs DB file
# - Look at the table structure
# - Execute a SQL query to return the most popular christmas song: Led Zeiplin's Stairway to Heaven.
#     select s.*, count (*) as pop from songs s, likes l WHERE s.id=l.songid GROUP BY l.songid ORDER BY pop DESC LIMIT 1;
# By sorting (highest first : descending), and limiting it to 1 result, we retrieve only the details of the most popular song


elf@5075a036ad10:~$ /usr/bin/sqlite3 
SQLite version 3.11.0 2016-02-15 17:29:24
Enter ".help" for usage hints.
Connected to a transient in-memory database.
Use ".open FILENAME" to reopen on a persistent database.

sqlite> .open christmassongs.db

sqlite> select * from sqlite_master;
table|songs|songs|2|CREATE TABLE songs(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  title TEXT,
  artist TEXT,
  year TEXT,
  notes TEXT
)
table|sqlite_sequence|sqlite_sequence|3|CREATE TABLE sqlite_sequence(name,seq)
table|likes|likes|4|CREATE TABLE likes(
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  like INTEGER,
  datetime INTEGER,
  songid INTEGER,
  FOREIGN KEY(songid) REFERENCES songs(id)
)


sqlite> select s.*, count (*) as pop from songs s, likes l WHERE s.id=l.songid GROUP BY l.songid ORDER BY pop DESC LIMIT 1;
392|Stairway to Heaven|Led Zeppelin|1971|"Stairway to Heaven" is a song by the English rock band Led Zeppelin, released in late 1971. It was composed by guitarist Jimmy Page and vocalist Robert Plant for the band's untitled fourth studio al
bum (often called Led Zeppelin IV). It is often referred to as one of the greatest rock songs of all time.|11325


# We then confirm that this is the correct answer:

elf@2715a4a4f2bb:~$ ./runtoanswer 
Starting up, please wait......
Enter the name of the song with the most likes: Stairway to Heaven
That is the #1 Christmas song, congratulations!


