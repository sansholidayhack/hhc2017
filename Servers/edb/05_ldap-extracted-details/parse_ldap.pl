#!/usr/bin/perl
use strict;
use warnings;

$/="]]";
open INFILE, "<$ARGV[0]" or die "Couldn't open file for reading\n$!\n";
while ( my $record = <INFILE> ) {
	my ($user)     = $record =~ /cn=(.*?),/;
	my ($email)    = $record =~ /mail\":\[\"(.*?)\"/;
	my ($password) = $record =~ /userPassword\":\[\"(.*?)\"/;
	next if !defined($user);
	#print "$record\n";
	print "$user\t$email\t$password\n";
}
