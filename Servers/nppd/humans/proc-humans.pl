#!/usr/bin/perl
use strict;
use warnings;
use Compress::Raw::Zlib;
use MIME::Base64;
# Author  : Paul Beckett
# Written : 29/12/2017
# Purpose : Parse humans.txt :
#             Convert Hex to ASCII
#             Extract raw zlib data
#             Base 64 decode
#             Extract text data from punch-card

my $ascii="";
my $b64="";
# Read in data
open (INFILE, "<$ARGV[0]") or die "Cannot open infile: $ARGV[0]\n$!\n";
while ( my $line=<INFILE> ) {
	next if $line !~/[A-Za-z0-9]{20}/;
	chomp $line;                 # Remove carriage returns and new lines
	$ascii.=pack "H*",$line;     # Convert to ASCII
}
# Extract data from Zlib:
my ($i, $status) = new Compress::Raw::Zlib::Inflate( -Bufsize => 300 ) ;
$status = $i->inflate($ascii, $b64);
#warn "$status";

# Base 64 decode;
my $punch = decode_base64($b64);
print $punch;

# Remove spaces;
$punch=~s/ ([0-9a-zA-Z# ])/$1/g;     # Remove spaces between columns
$punch=~s/^[|\s]*\n//mg;             # Remove spaces between rows
print $punch;
$punch=~s/[0-9]/ /g;                 # Remove numbering
print $punch;

###########################
# Convert punch card data #
###########################
$punch=~s/^[\/|_\s]*\n//mg;          # Remove card header & footer
$punch=~s/^\s*\///mg;                # Remove space from top left corner
$punch=~s/^\|\s//mg;                 # Remove left card edge
$punch=~s/\|$//mg;                   # Remove right card edge

my @punchElem;
my $rowCount=0;
for my $line (split /^/, $punch) {
	@{$punchElem[$rowCount++]}=split //, $line;
}
die "INVALID ROW COUNT: $rowCount\n" if $rowCount != 12;


# 029  &-0123456789ABCDEFGHIJKLMNOPQR/STUVWXYZ:#@'="¢.<(+|!$*);¬ ,%_>?
# IBME ¹-0123456789ABCDEFGHIJKLMNOPQR/STUVWXYZ:#²'="].<(+|[$*);¬³,%_>?
# EBCD &-0123456789ABCDEFGHIJKLMNOPQR/STUVWXYZ:#@'="[.<(+|]$*);^\,%_>?

my @EBCD;
$EBCD[0]='&-0123456789';
$EBCD[1]='ABCDEFGHI';
$EBCD[2]='JKLMNOPQR';
$EBCD[3]='/STUVWXYZ';
$EBCD[4]=':#@\'="';
$EBCD[5]='[.<(+|';
$EBCD[6]=']$*);^';
$EBCD[7]='\,%_>?';

for ( my $col=0 ; $col<80 ; $col++ ) {
	my $elements=0;
	my $index;
	my $prow;
	for ( my $row=0; $row<$rowCount; $row++ ) {
		if ( $punchElem[$row][$col] eq "#" ) {
			$elements++;
			if ( $row < 3 ) {
				warn "TWO INDEXES DEFINED: COL:$col : $index , $row\n" if defined $index;
				$index=$row;
			} else {
				warn "TWO VALUES DEFINED: COL:$col : $prow, $row\n" if defined $prow;
				$prow=$row;
			}
		}
	}
	if ( $elements == 0 ) {
		print " ";
	} elsif ( $elements > 2 ) {
		print "*";
		warn "Extended characters not handled!";
	} elsif ( $elements == 2 ) {
		print substr $EBCD[$index+1], ($prow-3), 1;
	} else {
		# Single Element from first string;
		print substr $EBCD[0], $prow, 1;
	}
	#warn "$col : $elements\n";
}



