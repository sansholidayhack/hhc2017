#!/usr/bin/perl
use strict;
use warnings;
use JSON;
# Author: Paul Beckett
# Purpose: Analyse data from SANS Holiday Hack 2017 challenge, to examine infraction distribution between nice and naughty individuals; and to identify moles.
#            - naughty_and_nice.csv file (obtained from SMB FileStor)
#            - JSON data of infractions : curl -i -s -k  -X 'GET' 'http://nppd.northpolechristmastown.com/infractions?query=date>2010-01-01&json=1' > nppd.date-2010-01-01.json

############################
# Load Naughty & Nice Data #
############################
my %nnlist;
open ( NNLIST, "<naughty_and_nice.csv" ) or die "Cannot open Naughty and Nice list\n$!\n";
while ( my $line=<NNLIST> ) {
	my ($name, $status)=$line=~/(^.*?),(Nice|Naughty)/;
	warn "Parsing Error : Unrecognised line : $line\n" if !defined $name;
	$nnlist{$name}{status}=$status;
}


##############################
# Load NPPD Infractions Data #
##############################
# Extracted with request : curl -i -s -k  -X 'GET' 'http://nppd.northpolechristmastown.com/infractions?query=date>2010-01-01&json=1' > nppd.date-2010-01-01.json
$/=undef;
open ( JSON, "<nppd.date-2010-01-01.json" ) or die "Cannot open infractions data file\n$!\n";
my $jsondata=<JSON>;
$/="\n";

my $infractions=decode_json($jsondata);
#print "Count : ${$infractions}{count}\n";
#print "Query : ${$infractions}{query}\n";

##############
# Merge Data #
##############
foreach my $ilist ( @{@{$infractions}{infractions}} ) {
	my $name     = ${$ilist}{name};

	my %offence;
	$offence{date}     = ${$ilist}{date};
	$offence{severity} = ${$ilist}{severity};
	$offence{status}   = ${$ilist}{status};
	$offence{title}    = ${$ilist}{title};
	$offence{coals}    = 0;
	$offence{coals}++ foreach ( @{${$ilist}{coals}} );

	my $outString = "$name\t$offence{date}\t$offence{severity}\t$offence{coals}\t$offence{status}\t$offence{title}";
	warn "Not on Naughty and Nice list: $outString\n" if !defined($nnlist{$name});
	warn "Coals not equal to severity: $outString\n" if $offence{coals} != $offence{severity};
	push @{$nnlist{$name}{offences}},\%offence;
}


####################
# Analyse & Output #
####################
my %naughtyListDist;
my %niceListDist;
# List Stats
foreach my $name ( keys %nnlist ) {
	#my $numOffences = (defined $nnlist{$name}{offences} ) ? length scalar(@{$nnlist{$name}{offences}}) : 0;
	my $numOffences=0;
	foreach my $offence ( @{$nnlist{$name}{offences}} ) {
		$numOffences++;
	}
	$niceListDist{$numOffences}++ if $nnlist{$name}{status} eq "Nice";
	$naughtyListDist{$numOffences}++ if $nnlist{$name}{status} eq "Naughty";
}
print "Nice List Distribution\n" . ( "-" x 60 ) . "\n";
foreach my $key ( sort {$a<=>$b} keys %niceListDist ) {
	print "$key\t$niceListDist{$key}\n";
}
print "Naughty List Distribution\n" . ( "-" x 60 ) . "\n";
foreach my $key ( sort {$a<=>$b} keys %naughtyListDist ) {
	print "$key\t$naughtyListDist{$key}\n";
}


# Find Moles
print "\nMoles\n" . ( "-" x 60 ) . "\n";
foreach my $name ( sort keys %nnlist ) {
	next if $nnlist{$name}{status} eq "Nice";
	my $offences="";
	my $moleOffence=0;
	foreach my $offence ( @{$nnlist{$name}{offences}} ) {
		$moleOffence++ if $$offence{title} =~ /super atomic wedgie|throwing rock.*?people|pulling of hair|unlicensed slingshot|playing with matches/i;
		$offences .= "\t$$offence{date}\t$$offence{status}\t$$offence{severity}\t$$offence{title}\n";
	}
	next if $moleOffence < 2;
	print "$name\t$nnlist{$name}{status}\n";
	print $offences;
}


