import json
import csv
import glob, os

merged_data = []
os.chdir("C:\\Users\\Alan\\Downloads\\SANS\\")
for file in glob.glob("*.json"):
    print(os.path.abspath(file))

    infractions = open(os.path.abspath(file),"r")
    infractions_parsed = json.load(infractions)
    infractions = infractions_parsed['infractions']
    merged_data = merged_data + infractions

# open a file for writing
csv_data = open('json.csv', 'w')
# create the csv writer object
csvwriter = csv.writer(csv_data, lineterminator='\n')
count = 0
for inf in merged_data:
      if count == 0:
             header = inf.keys()
             csvwriter.writerow(header)
             count += 1
      csvwriter.writerow(inf.values())
csv_data.close()