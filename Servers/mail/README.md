# Mail Server #
Hostname : mail.northpolechristmastown.com
IP : 10.142.0.5

/robots.txt reveals file: /cookies.txt , which contains JS code the site uses for authentication

fetchEmail.py was a useful script for dumping the emails in raw JSON, and also parsing the JSON to extract the fields we were interested in. The resulting extracted files can be seen in the extracted-emails folder

To test finding other accounts we created a variation on fetchEmail.py: checkEmailAddr.py, which accepts a filename (which should contain a list of names) which it will use to test for the existance of an email account, retrieving the email data if it exists. We checked to see if any of the 12 moles had email accounts in their names, but they didn't

