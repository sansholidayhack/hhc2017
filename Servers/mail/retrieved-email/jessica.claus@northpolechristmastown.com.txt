--------------------------------------------------
FOLDER: INBOX
--------------------------------------------------
	From    :admin@northpolechristmastown.com
	To      :jessica.claus@northpolechristmastown.com
	Subject :Welcome
	Hi,

Welcome to your new account.

--------------------------------------------------

	From    :"admin@northpolechristmastown.com" <admin@northpolechristmastown.com>
	To      :"jessica.claus@northpolechristmastown.com" <jessica.claus@northpolechristmastown.com>
	Subject :Re: Welcome
	Welcome ;)


On 11/15/2017 11:25 AM, jessica.claus@northpolechristmastown.com wrote:
> Oh joy, now Mr. Claus and I can keep in touch while he's delivery 
> presents.
>
>
> On 11/8/2017 11:03 AM, admin@northpolechristmastown.com wrote:
>> Hi,
>>
>> Welcome to your new account.
>


--------------------------------------------------

	From    :"alabaster.snowball@northpolechristmastown.com" <alabaster.snowball@northpolechristmastown.com>
	To      :"jessica.claus@northpolechristmastown.com" <jessica.claus@northpolechristmastown.com>
	Subject :gingerbread cookie recipe
	Hey Mrs Claus,

Do you have that awesome gingerbread cookie recipe you made for me last 
year? You sent it in a MS word .docx file. I would totally open that 
docx on my computer if you had that. I would click on anything with the 
words gingerbread cookie recipe in it. I'm totally addicted and want to 
make some more.

Thanks,
Alabaster Snowball

--------------------------------------------------

	From    :"alabaster.snowball@northpolechristmastown.com" <alabaster.snowball@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :COOKIES!
	Does anyone have any cookies left over from Mrs Claus cookie stock pile 
from last year? I'm working on the computer non-stop until Christmas 
doing development and desperately need some of her north pole famous 
gingerbread cookies to keep me going.

I already emailed her but for she is not in the North Pole.

I NEEEEED MOAR COOKIES!

-Alabaster Snowball


--------------------------------------------------

	From    :"santa.claus@northpolechristmastown.com" <santa.claus@northpolechristmastown.com>
	To      :"jessica.claus@northpolechristmastown.com" <jessica.claus@northpolechristmastown.com>
	Subject :Re: See you come December!
	Ok sounds good,

Have a good time!

Sincerely,

Nicholas


On 11/15/2017 1:12 PM, jessica.claus@northpolechristmastown.com wrote:
> Going to enjoy some time off Honey. You know the number to my sisters 
> place should you need it!
>
> Sincerely,
>
> Jessica
>


--------------------------------------------------

	From    :"mary.sugerplum@northpolechristmastown.com" <mary.sugerplum@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Re: COOKIES!
	Sorry, I dont know that recipe or have any left.


On 11/15/2017 1:10 PM, alabaster.snowball@northpolechristmastown.com wrote:
> Does anyone have any cookies left over from Mrs Claus cookie stock 
> pile from last year? I'm working on the computer non-stop until 
> Christmas doing development and desperately need some of her north 
> pole famous gingerbread cookies to keep me going.
>
> I already emailed her but for she is not in the North Pole.
>
> I NEEEEED MOAR COOKIES!
>
> -Alabaster Snowball
>


--------------------------------------------------

	From    :"sparkle.redberry@northpolechristmastown.com" <sparkle.redberry@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Re: COOKIES!
	Me neither, sorry.


On 11/15/2017 1:13 PM, mary.sugerplum@northpolechristmastown.com wrote:
> Sorry, I dont know that recipe or have any left.
>
>
> On 11/15/2017 1:10 PM, alabaster.snowball@northpolechristmastown.com 
> wrote:
>> Does anyone have any cookies left over from Mrs Claus cookie stock 
>> pile from last year? I'm working on the computer non-stop until 
>> Christmas doing development and desperately need some of her north 
>> pole famous gingerbread cookies to keep me going.
>>
>> I already emailed her but for she is not in the North Pole.
>>
>> I NEEEEED MOAR COOKIES!
>>
>> -Alabaster Snowball
>>
>


--------------------------------------------------

	From    :"pepper.minstix@northpolechristmastown.com" <pepper.minstix@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Re: COOKIES!
	I liked the raisin ones myself. Dont know about the gingerbread ones.


On 11/15/2017 1:14 PM, sparkle.redberry@northpolechristmastown.com wrote:
> Me neither, sorry.
>
>
> On 11/15/2017 1:13 PM, mary.sugerplum@northpolechristmastown.com wrote:
>> Sorry, I dont know that recipe or have any left.
>>
>>
>> On 11/15/2017 1:10 PM, alabaster.snowball@northpolechristmastown.com 
>> wrote:
>>> Does anyone have any cookies left over from Mrs Claus cookie stock 
>>> pile from last year? I'm working on the computer non-stop until 
>>> Christmas doing development and desperately need some of her north 
>>> pole famous gingerbread cookies to keep me going.
>>>
>>> I already emailed her but for she is not in the North Pole.
>>>
>>> I NEEEEED MOAR COOKIES!
>>>
>>> -Alabaster Snowball
>>>
>>
>


--------------------------------------------------

	From    :"tarpin.mcjinglehauser@northpolechristmastown.com" <tarpin.mcjinglehauser@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Re: COOKIES!
	Ewww, raisin. I loved the gingerbread cookies myself. I think that Mrs 
Claus gave me the recipe. If I find it, ill send it to you in an email. 
I believe it was a a MS Word docx file. So keep an eye out for an email 
containing the words "gingerbread" "cookie" "recipe" and a link or 
attachment to the .docx file.


On 11/15/2017 1:16 PM, pepper.minstix@northpolechristmastown.com wrote:
> I liked the raisin ones myself. Dont know about the gingerbread ones.
>
>
> On 11/15/2017 1:14 PM, sparkle.redberry@northpolechristmastown.com wrote:
>> Me neither, sorry.
>>
>>
>> On 11/15/2017 1:13 PM, mary.sugerplum@northpolechristmastown.com wrote:
>>> Sorry, I dont know that recipe or have any left.
>>>
>>>
>>> On 11/15/2017 1:10 PM, alabaster.snowball@northpolechristmastown.com 
>>> wrote:
>>>> Does anyone have any cookies left over from Mrs Claus cookie stock 
>>>> pile from last year? I'm working on the computer non-stop until 
>>>> Christmas doing development and desperately need some of her north 
>>>> pole famous gingerbread cookies to keep me going.
>>>>
>>>> I already emailed her but for she is not in the North Pole.
>>>>
>>>> I NEEEEED MOAR COOKIES!
>>>>
>>>> -Alabaster Snowball
>>>>
>>>
>>
>


--------------------------------------------------

	From    :"alabaster.snowball@northpolechristmastown.com" <alabaster.snowball@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Re: COOKIES!
	Awesome, yea if anyone finds that .docx file containing the recipe for 
"gingerbread cookie recipe", please send it to me in a docx file. Im 
currently working on my computer and would totally download that to my 
machine, open it, and click to all the prompts.


Thanks!

Alabaster Snowball.


On 11/15/2017 1:18 PM, tarpin.mcjinglehauser@northpolechristmastown.com 
wrote:
> Ewww, raisin. I loved the gingerbread cookies myself. I think that Mrs 
> Claus gave me the recipe. If I find it, ill send it to you in an 
> email. I believe it was a a MS Word docx file. So keep an eye out for 
> an email containing the words "gingerbread" "cookie" "recipe" and a 
> link or attachment to the .docx file.
>
>
> On 11/15/2017 1:16 PM, pepper.minstix@northpolechristmastown.com wrote:
>> I liked the raisin ones myself. Dont know about the gingerbread ones.
>>
>>
>> On 11/15/2017 1:14 PM, sparkle.redberry@northpolechristmastown.com 
>> wrote:
>>> Me neither, sorry.
>>>
>>>
>>> On 11/15/2017 1:13 PM, mary.sugerplum@northpolechristmastown.com wrote:
>>>> Sorry, I dont know that recipe or have any left.
>>>>
>>>>
>>>> On 11/15/2017 1:10 PM, 
>>>> alabaster.snowball@northpolechristmastown.com wrote:
>>>>> Does anyone have any cookies left over from Mrs Claus cookie stock 
>>>>> pile from last year? I'm working on the computer non-stop until 
>>>>> Christmas doing development and desperately need some of her north 
>>>>> pole famous gingerbread cookies to keep me going.
>>>>>
>>>>> I already emailed her but for she is not in the North Pole.
>>>>>
>>>>> I NEEEEED MOAR COOKIES!
>>>>>
>>>>> -Alabaster Snowball
>>>>>
>>>>
>>>
>>
>


--------------------------------------------------

	From    :"sparkle.redberry@northpolechristmastown.com" <sparkle.redberry@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Christmas Party!
	Hi Everyone!

Just a Reminder to everyone to mark their Calendars for the annual elf 
ball bash on December 25th to celebrate all of the hard work we put in 
over the past year. Ill be coordinating the party so let me know if you 
have any special requests!

Regards,

Sparkle Redberry


--------------------------------------------------

	From    :"holly.evergreen@northpolechristmastown.com" <holly.evergreen@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Re: Christmas Party!
	Yay! I cant wait. I just hope Tarpin Mcjinglehauser doesn't get sick off 
egg nog long he did last year.


On 11/15/2017 1:58 PM, sparkle.redberry@northpolechristmastown.com wrote:
> Hi Everyone!
>
> Just a Reminder to everyone to mark their Calendars for the annual elf 
> ball bash on December 25th to celebrate all of the hard work we put in 
> over the past year. Ill be coordinating the party so let me know if 
> you have any special requests!
>
> Regards,
>
> Sparkle Redberry
>


--------------------------------------------------

	From    :"tarpin.mcjinglehauser@northpolechristmastown.com" <tarpin.mcjinglehauser@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Re: Christmas Party!
	Yea..... Sorry about that!


On 11/15/2017 2:02 PM, holly.evergreen@northpolechristmastown.com wrote:
> Yay! I cant wait. I just hope Tarpin Mcjinglehauser doesn't get sick 
> off egg nog long he did last year.
>
>
> On 11/15/2017 1:58 PM, sparkle.redberry@northpolechristmastown.com wrote:
>> Hi Everyone!
>>
>> Just a Reminder to everyone to mark their Calendars for the annual 
>> elf ball bash on December 25th to celebrate all of the hard work we 
>> put in over the past year. Ill be coordinating the party so let me 
>> know if you have any special requests!
>>
>> Regards,
>>
>> Sparkle Redberry
>>
>


--------------------------------------------------

	From    :"sparkle.redberry@northpolechristmastown.com" <sparkle.redberry@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Re: Christmas Party!
	Awesome, this is going to be a blast


On 11/15/2017 1:58 PM, sparkle.redberry@northpolechristmastown.com wrote:
> Hi Everyone!
>
> Just a Reminder to everyone to mark their Calendars for the annual elf 
> ball bash on December 25th to celebrate all of the hard work we put in 
> over the past year. Ill be coordinating the party so let me know if 
> you have any special requests!
>
> Regards,
>
> Sparkle Redberry
>


--------------------------------------------------

	From    :"wunorse.openslae@northpolechristmastown.com" <wunorse.openslae@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Re: Christmas Party!
	Yea, I cant wait either. Ill be bringing the chips!


On 11/15/2017 2:04 PM, sparkle.redberry@northpolechristmastown.com wrote:
> Awesome, this is going to be a blast
>
>
> On 11/15/2017 1:58 PM, sparkle.redberry@northpolechristmastown.com wrote:
>> Hi Everyone!
>>
>> Just a Reminder to everyone to mark their Calendars for the annual 
>> elf ball bash on December 25th to celebrate all of the hard work we 
>> put in over the past year. Ill be coordinating the party so let me 
>> know if you have any special requests!
>>
>> Regards,
>>
>> Sparkle Redberry
>>
>


--------------------------------------------------

	From    :"minty.candycane@northpolechristmastown.com" <minty.candycane@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Should we be worried?
	Hey Alabaster,

You know I'm a novice security enthusiast, well I saw an article a while 
ago about regarding DDE exploits that dont need macros for MS word to 
get command execution.

https://sensepost.com/blog/2017/macro-less-code-exec-in-msword/

Should we be worried about this?

I tried it on my local machine and was able to transfer a file. Here's a 
poc:

http://mail.northpolechristmastown.com/attachments/dde_exmaple_minty_candycane.png

I know your the resident computer engineer here so I wanted to defer to 
the expert.

:)

-Minty CandyCane.


--------------------------------------------------

	From    :"alabaster.snowball@northpolechristmastown.com" <alabaster.snowball@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Re: Should we be worried?
	Quit worrying Minty,

You have nothing to worry about with me around! I have developed most of 
the applications in our network including our network defenses. We are 
are completely secure and impenetrable.

Sincerely,

Alabaster Snowball.


On 11/15/2017 2:41 PM, minty.candycane@northpolechristmastown.com wrote:
> Hey Alabaster,
>
> You know I'm a novice security enthusiast, well I saw an article a 
> while ago about regarding DDE exploits that dont need macros for MS 
> word to get command execution.
>
> https://sensepost.com/blog/2017/macro-less-code-exec-in-msword/
>
> Should we be worried about this?
>
> I tried it on my local machine and was able to transfer a file. Here's 
> a poc:
>
> http://mail.northpolechristmastown.com/attachments/dde_exmaple_minty_candycane.png 
>
>
> I know your the resident computer engineer here so I wanted to defer 
> to the expert.
>
> :)
>
> -Minty CandyCane.
>


--------------------------------------------------

	From    :"minty.candycane@northpolechristmastown.com" <minty.candycane@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Re: Should we be worried?
	This is a multi-part message in MIME format.
--------------1BE15AD45E8A2D947D3B4FFE
Content-Type: text/plain; charset=utf-8; format=flowed
Content-Transfer-Encoding: 8bit

¯\_(ツ)_/¯

Good enough for me!


On 11/15/2017 2:45 PM, alabaster.snowball@northpolechristmastown.com wrote:
> Quit worrying Minty,
>
> You have nothing to worry about with me around! I have developed most 
> of the applications in our network including our network defenses. We 
> are are completely secure and impenetrable.
>
> Sincerely,
>
> Alabaster Snowball.
>
>
> On 11/15/2017 2:41 PM, minty.candycane@northpolechristmastown.com wrote:
>> Hey Alabaster,
>>
>> You know I'm a novice security enthusiast, well I saw an article a 
>> while ago about regarding DDE exploits that dont need macros for MS 
>> word to get command execution.
>>
>> https://sensepost.com/blog/2017/macro-less-code-exec-in-msword/
>>
>> Should we be worried about this?
>>
>> I tried it on my local machine and was able to transfer a file. 
>> Here's a poc:
>>
>> http://mail.northpolechristmastown.com/attachments/dde_exmaple_minty_candycane.png 
>>
>>
>> I know your the resident computer engineer here so I wanted to defer 
>> to the expert.
>>
>> :)
>>
>> -Minty CandyCane.
>>
>


--------------1BE15AD45E8A2D947D3B4FFE
Content-Type: text/html; charset=utf-8
Content-Transfer-Encoding: 8bit

<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  </head>
  <body text="#000000" bgcolor="#FFFFFF">
    <p><span style="color: rgb(0, 0, 0); font-family: &quot;Helvetica
        Neue&quot;, Helvetica, Arial, sans-serif; font-size: 16px;
        font-style: normal; font-variant-ligatures: normal;
        font-variant-caps: normal; font-weight: normal; letter-spacing:
        normal; orphans: 2; text-align: start; text-indent: 0px;
        text-transform: none; white-space: normal; widows: 2;
        word-spacing: 0px; -webkit-text-stroke-width: 0px;
        text-decoration-style: initial; text-decoration-color: initial;
        display: inline !important; float: none;">¯\_(ツ)_/¯</span></p>
    <p><span style="color: rgb(0, 0, 0); font-family: &quot;Helvetica
        Neue&quot;, Helvetica, Arial, sans-serif; font-size: 16px;
        font-style: normal; font-variant-ligatures: normal;
        font-variant-caps: normal; font-weight: normal; letter-spacing:
        normal; orphans: 2; text-align: start; text-indent: 0px;
        text-transform: none; white-space: normal; widows: 2;
        word-spacing: 0px; -webkit-text-stroke-width: 0px;
        text-decoration-style: initial; text-decoration-color: initial;
        display: inline !important; float: none;">Good enough for me!<br>
      </span></p>
    <br>
    <div class="moz-cite-prefix">On 11/15/2017 2:45 PM,
      <a class="moz-txt-link-abbreviated" href="mailto:alabaster.snowball@northpolechristmastown.com">alabaster.snowball@northpolechristmastown.com</a> wrote:<br>
    </div>
    <blockquote type="cite"
cite="mid:940ba863-3a1a-d531-b0d7-232d44f79988@northpolechristmastown.com">Quit
      worrying Minty,
      <br>
      <br>
      You have nothing to worry about with me around! I have developed
      most of the applications in our network including our network
      defenses. We are are completely secure and impenetrable.
      <br>
      <br>
      Sincerely,
      <br>
      <br>
      Alabaster Snowball.
      <br>
      <br>
      <br>
      On 11/15/2017 2:41 PM, <a class="moz-txt-link-abbreviated" href="mailto:minty.candycane@northpolechristmastown.com">minty.candycane@northpolechristmastown.com</a>
      wrote:
      <br>
      <blockquote type="cite">Hey Alabaster,
        <br>
        <br>
        You know I'm a novice security enthusiast, well I saw an article
        a while ago about regarding DDE exploits that dont need macros
        for MS word to get command execution.
        <br>
        <br>
        <a class="moz-txt-link-freetext" href="https://sensepost.com/blog/2017/macro-less-code-exec-in-msword/">https://sensepost.com/blog/2017/macro-less-code-exec-in-msword/</a>
        <br>
        <br>
        Should we be worried about this?
        <br>
        <br>
        I tried it on my local machine and was able to transfer a file.
        Here's a poc:
        <br>
        <br>
<a class="moz-txt-link-freetext" href="http://mail.northpolechristmastown.com/attachments/dde_exmaple_minty_candycane.png">http://mail.northpolechristmastown.com/attachments/dde_exmaple_minty_candycane.png</a>
        <br>
        <br>
        I know your the resident computer engineer here so I wanted to
        defer to the expert.
        <br>
        <br>
        :)
        <br>
        <br>
        -Minty CandyCane.
        <br>
        <br>
      </blockquote>
      <br>
    </blockquote>
    <br>
  </body>
</html>

--------------1BE15AD45E8A2D947D3B4FFE--

--------------------------------------------------

	From    :"holly.evergreen@northpolechristmastown.com" <holly.evergreen@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Lost book page
	Hey Santa,

Found this lying around. Figured you needed it.

http://mail.northpolechristmastown.com/attachments/GreatBookPage4_893jt91md2.pdf

:)

-Holly


--------------------------------------------------

	From    :"mary.sugerplum@northpolechristmastown.com" <mary.sugerplum@northpolechristmastown.com>
	To      :"holly.evergreen@northpolechristmastown.com" <holly.evergreen@northpolechristmastown.com>, all@northpolechristmastown.com
	Subject :Re: Lost book page
	Ill save it for Santa along with the other one I have on my computer. 
Pulling it down now via nc.exe "like a boss".


On 12/5/2017 9:10 AM, holly.evergreen@northpolechristmastown.com wrote:
> Hey Santa,
>
> Found this lying around. Figured you needed it.
>
> http://mail.northpolechristmastown.com/attachments/GreatBookPage4_893jt91md2.pdf 
>
>
> :)
>
> -Holly
>


--------------------------------------------------

	From    :"alabaster.snowball@northpolechristmastown.com" <alabaster.snowball@northpolechristmastown.com>
	To      :all@northpolechristmastown.com
	Subject :Re: Lost book page
	Well powershell is my new love but netcat will always hold a special 
place in my heart.


On 12/5/2017 2:32 PM, minty.candycane@northpolechristmastown.com wrote:
> You still use netcat on your windows box?
>
>
> On 12/5/2017 2:31 PM, mary.sugerplum@northpolechristmastown.com wrote:
>> Ill save it for Santa along with the other one I have on my computer. 
>> Pulling it down now via nc.exe "like a boss".
>>
>>
>> On 12/5/2017 9:10 AM, holly.evergreen@northpolechristmastown.com wrote:
>>> Hey Santa,
>>>
>>> Found this lying around. Figured you needed it.
>>>
>>> http://mail.northpolechristmastown.com/attachments/GreatBookPage4_893jt91md2.pdf 
>>>
>>>
>>> :)
>>>
>>> -Holly
>>>
>>
>


--------------------------------------------------

	From    :alabaster.snowball@northpolechristmastown.com
	To      :all@northpolechristmastown.com
	Subject :Re: Lost book page
	On 12/12/2017 2:20 PM, alabaster.snowball@northpolechristmastown.com wrote:

I installed nc.exe to path for my computer.


On 12/5/2017 2:33 PM, alabaster.snowball@northpolechristmastown.com wrote:
> Well powershell is my new love but netcat will always hold a special 
> place in my heart.
>
>
> On 12/5/2017 2:32 PM, minty.candycane@northpolechristmastown.com wrote:
>> You still use netcat on your windows box?
>>
>>
>> On 12/5/2017 2:31 PM, alabaster_snowball@northpolechristmastown.com 
>> wrote:
>>> Ill save it for Santa along with the other one I have on my 
>>> computer. Pulling it down now via nc.exe "like a boss".
>>>
>>>
>>> On 12/5/2017 9:10 AM, holly.evergreen@northpolechristmastown.com wrote:
>>>> Hey Santa,
>>>>
>>>> Found this lying around. Figured you needed it.
>>>>
>>>> http://mail.northpolechristmastown.com/attachments/GreatBookPage4_893jt91md2.pdf 
>>>>
>>>>
>>>> :)
>>>>
>>>> -Holly
>>>>
>>>
>>
>


--------------------------------------------------

--------------------------------------------------
FOLDER: SENT
--------------------------------------------------
	From    :"jessica.claus@northpolechristmastown.com" <jessica.claus@northpolechristmastown.com>
	To      :admin@northpolechristmastown.com
	Subject :Re: Welcome
	Oh joy, now Mr. Claus and I can keep in touch while he's delivery presents.


On 11/8/2017 11:03 AM, admin@northpolechristmastown.com wrote:
> Hi,
>
> Welcome to your new account.


--------------------------------------------------

	From    :"jessica.claus@northpolechristmastown.com" <jessica.claus@northpolechristmastown.com>
	To      :"alabaster.snowball@northpolechristmastown.com" <alabaster.snowball@northpolechristmastown.com>
	Subject :Re: gingerbread cookie recipe
	Thank you for your email. I’m out of the the North Pole visiting my 
sister in Florida and will be back on December 23. During this period I 
will have limited access to my email.

Please call me if need anything

Sincerely,

Jessica Claus


On 11/15/2017 1:04 PM, alabaster.snowball@northpolechristmastown.com wrote:
> Hey Mrs Claus,
>
> Do you have that awesome gingerbread cookie recipe you made for me 
> last year? You sent it in a MS word .docx file. I would totally open 
> that docx on my computer if you had that. I would click on anything 
> with the words gingerbread cookie recipe in it. I'm totally addicted 
> and want to make some more.
>
> Thanks,
> Alabaster Snowball


--------------------------------------------------

	From    :"jessica.claus@northpolechristmastown.com" <jessica.claus@northpolechristmastown.com>
	To      :"santa.claus@northpolechristmastown.com" <santa.claus@northpolechristmastown.com>
	Subject :See you come December!
	Going to enjoy some time off Honey. You know the number to my sisters 
place should you need it!

Sincerely,

Jessica


--------------------------------------------------

